package com.example.mateusz.sensorreader;

/**
 * Created by mateusz on 29/04/18.
 */

public class Constants {

    public interface ACTIONS {
        public static final String START_SERVICE_ACTION = "StartSendingService";
        public static final String CONTINUE_SERVICE_ACTION = "ContinueSendingService";
    }

    public interface NOTIFICATION_IDS {
        public static final int ALERT_NOTIFICATION = 42;
        public static final int SERVICE_NOTIFICATION = 43;
    }
}
