package com.example.mateusz.sensorreader;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import java.util.zip.DataFormatException;

public class MainActivity extends AppCompatActivity {

    private EditText nodeNameDisplay;
    private EditText periodDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nodeNameDisplay = findViewById(R.id.nodeName);
        periodDisplay = findViewById(R.id.sendingPeriod);
    }

    public void sendData(View view) {
        Intent displayActivityIntent = new Intent(this, DisplayActivity.class);
        String nodeName;
        int sendingPeriod;
        try {
            nodeName = getName();
        } catch (DataFormatException e) {
            nodeNameDisplay.setError(getString(R.string.invalidNameError));
            return;
        }
        try {
            sendingPeriod = getPeriod();
        } catch (DataFormatException e){
            periodDisplay.setError(getString(R.string.invalidPeriodError));
            return;
        }
        displayActivityIntent.setAction(Constants.ACTIONS.START_SERVICE_ACTION);
        displayActivityIntent.putExtra("name", nodeName);
        displayActivityIntent.putExtra("period", sendingPeriod);
        startActivity(displayActivityIntent);
    }

    private String getName() throws DataFormatException {
        String name = nodeNameDisplay.getText().toString();
        if (!name.isEmpty()) {
            return name;
        } else {
            throw new DataFormatException();
        }
    }

    private int getPeriod() throws DataFormatException {
        int period = Integer.parseInt(periodDisplay.getText().toString());
        if (period >= 100) {
            return period;
        } else {
            throw new DataFormatException();
        }
    }

}
